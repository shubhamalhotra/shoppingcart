package com.shopping.cart;

import java.util.ArrayList;
import java.util.List;

public class ShoppingList {

    public static void main(String[] args)
    {
        Owner owner = new Owner("Shubha");

        Catalogue catalogue = createCatalogue(owner);
        User user = new User(catalogue);
        Product productZaraItemClothes =  user.selectItem("ZARA","Clothes");
        System.out.println(productZaraItemClothes.getName());
    }

    public static Catalogue createCatalogue(Owner owner)
    {
        Catalogue catalogue = null;
        {
            Brand brandZara = new Brand();
            brandZara.setName("ZARA");
            Product productZaraClothes = new Product();
            productZaraClothes.setName("Clothes");

            Product productZaraFootwear = new Product();
            productZaraFootwear.setName("Footwear");

            Product productZaraBags = new Product();
            productZaraBags.setName("Bags");

            List productZaraList = new ArrayList<>();
            productZaraList.add(productZaraClothes);
            productZaraList.add(productZaraFootwear);
            productZaraList.add(productZaraBags);
            brandZara.setProductsList(productZaraList);

            owner.createCatalogue(brandZara);
        }

        {
            Brand brandGap = new Brand();
            brandGap.setName("GAP");
            Product productGapClothes = new Product();
            productGapClothes.setName("Clothes");

            Product productGapFootwear = new Product();
            productGapFootwear.setName("Footwear");

            Product productGapBags = new Product();
            productGapBags.setName("Bags");

            List productGapList = new ArrayList<>();
            productGapList.add(productGapClothes);
            productGapList.add(productGapFootwear);
            productGapList.add(productGapBags);
            brandGap.setProductsList(productGapList);

            catalogue = owner.createCatalogue((brandGap));
        }

        return catalogue;


    }
}
