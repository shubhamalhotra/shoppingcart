package com.shopping.cart;

import java.util.ArrayList;
import java.util.List;

public class Brand {

    private String name;
    private List<Product> productsList = new ArrayList<>();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setProductsList(List<Product> productsList) {
        this.productsList = productsList;
    }

    public List<Product> getProductsList() {
        return productsList;
    }
}