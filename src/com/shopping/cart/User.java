package com.shopping.cart;

import java.util.Iterator;
import java.util.List;

public class User {

    private Catalogue catalogue = null;
    public User(Catalogue catalogue)
    {
        this.catalogue = catalogue;
    }

    public void displayItems(){
        catalogue.getBrandProductMap();
    }

    public Product selectItem(String brandName, String productName){
        List<Product> productList = catalogue.getBrandProductMap().get(brandName);
        for(Iterator<Product> productIterator = productList.iterator();productIterator.hasNext();){
            Product product = productIterator.next();
            if(productName.equalsIgnoreCase(product.getName()))
            {
                return product;
            }
        }


        return null;
    }

}
