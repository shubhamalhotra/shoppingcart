package com.shopping.cart;
import java.util.*;

public class Catalogue {

    Map<String, List<Product>> brandProductMap = new HashMap<String, List<Product>>();

    public Map<String, List<Product>> getBrandProductMap() {
        return brandProductMap;
    }

    public void addBrand(Brand brand) {

        String brandName = brand.getName();
        List productList =brand.getProductsList() ;
        if(brandProductMap.containsKey(brandName))
        {
            List existingProductList =  brandProductMap.get(brandName);
            existingProductList.addAll(productList);
        }
        else {
            brandProductMap.put(brandName, productList);
        }
            System.out.println("Brand is " +brandName);
        for(Iterator<Product> itProduct = productList.iterator(); itProduct.hasNext();)
        {
            Product product =  itProduct.next();
            System.out.println("Product " + product.getName());
        }
    }
}
